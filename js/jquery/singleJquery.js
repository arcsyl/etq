

var currentFigureId = 1;

function isVisible( row, container ){

  var elementTop = $(row).offset().top,
      elementHeight = $(row).height(),
      containerTop = container.scrollTop(),
      containerHeight = container.height();

  return ((((elementTop - containerTop) + elementHeight) > 0) && ((elementTop - containerTop) < containerHeight));
  }
  $(window).scroll(function(){
    $('#js-page-view div').each(function(){
        if(isVisible($("#fitImages"), $(window))){
          if ($('#info').hasClass("fixed")) {
          $('#info').removeClass("fixed");
          }
        } else {
          if ($('#info').hasClass("fixed")) {} else {
              $('#info').addClass("fixed");
          }
        }
    });
    $(".image-container figure").each(function() {
      if(isVisible($(this), $(window))) {
        currentFigureId = $(this).attr('id');
      }
    });
    if (currentFigureId == 5) {
      $('.next').addClass('disabled');
    } else {
      if ($('.next').hasClass('disabled')) {
        $('.next').removeClass('disabled');
      }
    }
    if (currentFigureId > 1) {
      $('.prev').removeClass('disabled');
    } else {
      if (!$('.prev').hasClass('disabled')) {
        $('.prev').addClass('disabled');
      }
    }
  });


  $('figure').click(function() {
    if ($('.image-container').hasClass("zoomedin")) {
      $('.image-container').removeClass("zoomedin");
      $('#info').removeClass("slide-away");
    } else {
      $('.image-container').addClass("zoomedin");
      $('#info').addClass("slide-away");
    }

  });
  $('.hide-for-medium-portrait .items').click(function() {
    if ($('.image-container').hasClass("zoomedin")) {
      $('.image-container').removeClass("zoomedin");
      $('#info').removeClass("slide-away");
      $('.hide-for-medium-portrait .items').removeClass('zoom-out');
    } else {
      $('.image-container').addClass("zoomedin");
      $('#info').addClass("slide-away");
      $('.hide-for-medium-portrait .items').addClass('zoom-out');
    }

  });



  var nextFigure = 1;
  $('.next').click(function() {
    nextFigure = ++currentFigureId;
    console.log(nextFigure);
    $('html, body').animate({
        scrollTop: $("#"+nextFigure).offset().top
    }, 2000);
  });
  $('.prev').click(function() {
    nextFigure = --currentFigureId;
    console.log(nextFigure);
    $('html, body').animate({
        scrollTop: $("#"+nextFigure).offset().top
    }, 2000);
  });



  var textBtn = "";
  $('.sizeBtn').click(function() {
    if ($('#product-sizes').hasClass('hide')) {
      $('#product-sizes').removeClass('hide');
    } else {
      $('#product-sizes').addClass('hide');
    }
  });
  $('.filterList .size').click(function(e) {
    if ($(this).attr('id') == "notifyclick") {
      $('#notifywaitlist').removeClass('hide');
      $('.ui-close-waitlist').click(function() {
        $('#notifywaitlist').addClass('hide');
      });
    } else {
      $('.filterList .size').removeClass("selected");
      $(this).addClass("selected");
      textBtn = $(e.target).text();
      $('.sizeBtn strong').text(textBtn);
      $('#product-sizes').addClass('hide');
      $('.footerbuttons .inverse').removeClass('disabled');
    }
  });

  $.extend({
    el: function(el, props) {
        var $el = $(document.createElement(el));
        if (props !== null) {
          $el.attr(props);
        }
        return $el;
    }
  });

  var cartCount = 0;
  var totalPrice = 0;
  var cartId = 1;

  $('.addbtn').click(function() {
    cartCount = $('.cart-products li').length;
    ++cartCount;

    // $($('#addToCart').first().clone()).appendTo(".cart-products");

    $('.cart-products').append(
      $.el('li',{'product-id':'7958'}).append(
        $.el('figure',null).append(
          $.el('img',{'width':'300','height':'300','src':'img/Low-white-stacked/small.jpg','class':'attachment-medium size-medium wp-post-image','alt':'Low-1-Stacked','srcset':'img/Low-white-stacked/small.jpg 300w, img/Low-white-stacked/small.jpg 150w, img/Low-white-stacked/large.jpg 768w, img/Low-white-stacked/small.jpg 180w, img/Low-white-stacked/medium.jpg 600w, img/Low-white-stacked/large.jpg 1024w','sizes':'max-width: 300px) 100vw, 300px'})
        )
      )
      .append(
        $.el('div',{'data-count':'1'}).append(
          $.el('h3').text('Low 1 White Stacked')
        )
        .append(
          $.el('p').text('EU 42 | US 9 | UK 8 QTY: ').append(
            $.el('span',{'class':'count'}).text('1')
          )
        )
        .append(
          $.el('h4').text('€ 250.00')
        )
        .append(
          $.el('button',{'class':'remove-product'}).text('x')
        )
      )
    );
    cartId++;


    $('.cartcount').text(cartCount);
    $('.cart-products').css({"opacity": ""});
    $('.cart-products li').css({"visibility": ""});

    function calculateTotalCartPrice() {
      var sum = 0;
      $('.cart-products li h4').each(function() {
        var val = $.trim( $(this).text() );

        if ( val ) {
            val = parseFloat( val.replace( /€/, "" ) );
            sum += !isNaN( val ) ? val : 0;
        }
      });
        return sum;
     }

    $(".remove-product").click(function() {
      $(this).parent().parent().remove();
      totalPrice = calculateTotalCartPrice();
      $('#totalPrice').text('€ ' + totalPrice);
      cartCount = $('.cart-products li').length;
      $('.cartcount').text(cartCount);
    });

    totalPrice = calculateTotalCartPrice();

    $('#totalPrice').text('€ ' + totalPrice);
    totalPrice = 0;
  });
